(function($){
  $(function(){

    /*
    side navigation bar initialisation
    */
    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
      }
    );

    /* Vis.js network configuration settings. All configuration variables are prefixed and suffixed with __ */
/*    __options__ = {
      height: '300px',
      width: '100%',
      layout: {
        hierarchical: {
          direction: "DU",
          sortMethod: "directed"
        }
      },
      edges: {
        smooth: false
      },
      interaction: {
        dragNodes: false,
        navigationButtons: true
      },
      physics: {
        enabled: false
      }
    };*/

    function deepcopy(obj) {
      //should improve it.
      return JSON.parse(JSON.stringify(obj));
    }

    __options__ = {
      height: "100%",
      width: "100%",
      nodes: {
        //            shape: 'dot',
        //            size: 30,
        //            font: {
        //                size: 32
        //            },
        //            borderWidth: 2,
        shadow: true,
        group: "ndefault"
      },
      edges: {
        //            width: 2,
        shadow: true,
        font: { align: "bottom" },
        arrows: "to"
      },
      interaction: {
        dragNodes: true,
        navigationButtons: true,
        multiSelect: true
      },
      physics: {
        enabled: true
      },
      layout: {
        //random seed is to keep network rendoring same for same data.
        randomSeed: 2798872977430976
      },
      groups: {
        ndefault: {
          size: 6,
          color: {
            border: "#2B7CE9",
            background: "#97C2FC",
            highlight: {
              border: "#e43814",
              background: "#ff5549"
            },
            hover: {
              border: "#2B7CE9",
              background: "#D2E5FF"
            }
          },
          borderWidth: 1,
          borderWidthSelected: 1
        },
        disabled: {
          color: {
            border: "#aaaaaa",
            background: "#cccccc",
            highlight: {
              border: "#e43814",
              background: "#ff5549"
            }
          },
          font: {
            color: "#666666"
          },
          borderWidth: 1,
          borderWidthSelected: 1
        },
        base: {
          color: {
            border: "#af974f",
            //"background": "#f1eecd",
            background: "#f1d98b",
            highlight: {
              border: "#e43814",
              background: "#ff5549"
            }
          },
          font: {
            color: "#666666"
          },
          borderWidth: 1,
          borderWidthSelected: 1
        },
        cluster: {
          color: {
            background: "white",
            border: "#2B7CE9"
          },
          font: { size: 30 }
        }
      }
    };

    $(".tabs").tabs();



    /* All variables with a $_ prefix are Jquery Objects corresponding to HTML Elements */
    $_networks = $(".mynetwork");
    $_shaastraText = $("#shaastra-text");

    $_vakyaContent = $("#content");
    $_vakyaContentStatus = $("#vakya-content-status");
    $_contentWrapper = $("#content-wrapper");
    $_contentNe = $("#content-ne");
    $_vakyaContentNodes = $("#vakya-content-nodes");
    $_vakyaContentEdges = $("#vakya-content-edges");

    $_contentArea = $("#content-area");
    $_preloader = $("#preloader");
    $_searchInput = $("#search-input");
    $_searchResultsList = $("#search-results-list");
    $_searchResultsStatus = $("#search-results-status");
    $_searchResultsContainer = $("#search-results-container");

    $_statusbar = $("#statusbar");
    $_statusbarStatus = $("#statusbar-status");
    $_refreshButton = $("#refresh-button");
    $_errorReport = $("#error-report");
//    $_repoSetter = $("#repo-selector");
    $_shaastraList = $("#shaastra-list");
    $_listRefreshButton = $("#list-refresh-button");
    $_listErrorReport = $("#list-error-report");
    $_listStatus = $("#list-status");
    $_listSearchInput = $("#list-search-input")

    function getPresentEVis() {
        if (activeTabId == '#si-card-tab-padarthas') {
            var $EVis = $EVis0;
        }
        else if (activeTabId == '#si-card-tab-vakyas') {
            var $EVis = $EVis1;
        }
        else {
            var $EVis = null;
        }
        return $EVis;
    }

      $("#cluster-by-color").click(() => {
          let $EVis = getPresentEVis();
          if($EVis) {
              $EVis.clusterByColor();
          }
      });

      $("#cluster-by-hubsize").click(() => {
          let $EVis = getPresentEVis();
          if ($EVis) {
              $EVis.clusterBySequences();
          }
      });

      $("#cluster-by-none").click(() => {
          let $EVis = getPresentEVis();
          if ($EVis) {
            $EVis.clusterByNone();
          }
      });

      $("#cluster-by-outliers").click(() => {
          let $EVis = getPresentEVis();
          if ($EVis) {
              $EVis.clusterByOutLiers();
          }
      });

    /*
    Fallowing are variables for shaastra list data, like id of shaastra selected, and map of all shaastraIds to shaastraNames, etc.
    */

    var currentShaastraId = '';
    var id2ShaastraMap = {} ;//to conviniently map ids to names, derived from shaastraList var
    var apiPrefix =  $API_URL_PREFIX;

    var networkSelectionInfo = null;
    padarthaGraphSelectionInfo = null;


    var allTabIds = ["#si-card-tab-padarthas", "#si-card-tab-vakyas", "#si-card-tab-sections"];
    var activeTabId = "#si-card-tab-padarthas";
    var padarthasTabShown = false;
    var VakyasTabShown = false;
    var sectionsTabShown = false;
    var graphTitles = ['Padarthas', 'Vakyas', 'Sections']


    var currentPadarthaId = null;
    var padarthasRendered = false;
     padarthaResult = null;
    padarthaVakyaMap = null;
    var allPadarthaGraphResult = null;

    var AllVakyasResult = null;
    

    function resetPadarthas() {
      currentPadarthaId = null;
      padarthaGraphSelectionInfo = {};
      padarthasRendered = false;
      padarthaResult = null;
      padarthaVakyaMap = null;
      allPadarthaGraphResult = null;
      graphTitles[0] = "Padarthas";
      padarthasTabShown = false;
      $("#padartha-graph-nodes-list").html("");
      $("#padartha-list").html("");
      $_networks.eq(0).html("");
      $EVis0 = null;
      $EVis_svt = null;
      $("#samanya-vishesha-tree").html("");
    }

    function resetVakyas() {
      VakyasTabShown = false;
      graphTitles[1] = 'Vakyas';
      AllVakyasResult = null;
      $_networks.eq(1).html("");
      graphTitles[1] = "All Vakyas"
      $EVis1 = null;
    }

    function resetSections() {
      sectionsTabShown = false;
      graphTitles[2] = 'Sections';
    }

    function resetSelectionSection() {
      networkSelectionInfo = {};
      networkSelectionItems= null;
      $(".selected-count").text("");
      $(".ci-list").html("");
      $("#ci-network").html("");
      $("#vakya-content-nodes").html("");
      $("#vakya-content-edges").html("");
      $(".ci-list-div").show();
      $("#content").hide();
      $EVis_sel = null;
    }

    function resetSearchArea() {
      $_searchResultsStatus.html("");
      $_searchResultsList.html("");
      $_searchInput.val("");
      $_searchResultsContainer.hide();
    }

    function resetGlobal() {
      activeTabId = "#si-card-tab-padarthas";
      resetSelectionSection();
      resetSearchArea();
      $EVis0 = $EVis1 = $EVis_sel = null;
      $("#si-card-tab-padarthas-link").click();
      $("#padartha-graph-type").val("default").change();

    }

    function docsUrlForSpreadsheet(spreadsheetId) {
      return "https://docs.google.com/spreadsheets/d/" + spreadsheetId + "/edit";
    }

    $_networks.hide();

    function onclickForListItem(g) {
      return function() {
        $_listSearchInput.val("");
        $_shaastraList.children().each(function(i) {
          $(this).show();
        })
        currentShaastraId = g.Book_id;
        currentShaastra = g;
        $(".clicked-list-item").removeClass("clicked-list-item");
        $(this).addClass("clicked-list-item");
        //loadGraph();
        resetGlobal();
        resetPadarthas();
        resetVakyas();
        resetSections();
        document.title = id2ShaastraMap[currentShaastraId]["Book_title"] + " - " + "Shaastra-Maps";
        loadAppropriate();
      }
    }

    function loadAppropriate() {
      // loadGraph();
      $(activeTabId).click();
    }

    function handleNetworkSwitch() {
      $_networks.hide();
      let current_tab_index = allTabIds.indexOf(activeTabId);
      $("#graph-title").text(graphTitles[current_tab_index]);
      $_networks.eq(current_tab_index).show();
    }

    $_refreshButton.click(function() {
        $_statusbarStatus.html('pls wait. re-importing from spreadsheet.....');
        let importUrl = `${$API_URL_PREFIX}import/granthas/${currentShaastraId}?idType=Book_id&overwrite=1&skip_errors=1`;
        $.ajax({
          url:importUrl,
            method: 'POST',
            xhrFields: { withCredentials: true },
          success: () => {
              onclickForListItem(
                id2ShaastraMap[currentShaastraId]
              )();
          },
          error: () => {
              $_statusbarStatus.text('error in importing shaastra from spreadsheet.');
          }
        });
      
    });

/*
    $_repoSetter.change(function() {
        var selected_repo_name = $_repoSetter.val();
        var data = new FormData()
        data.append("repo_name", selected_repo_name);
        $.ajax({
            url: REPO_URL,
            type: "POST",
            data: data,
            xhrFields: { withCredentials: true },
            processData: false,
            contentType: false,
            success: function(result) {
                loadGranthas();
            }
        });
    });
 */

    var selected_repo_name = "vedavaapi";
    var data = new FormData()
    data.append("repo_name", selected_repo_name);
    $.ajax({
        url: REPO_URL,
        type: "POST",
        data: data,
        xhrFields: { withCredentials: true },
        processData: false,
        contentType: false,
        success: function(result) {
            loadGranthas();
        }
    });

/*
    $_listRefreshButton.click(function() {
      $_listSearchInput.val("");
      loadGranthas();
    });
*/

    $_listRefreshButton.click(function() {
        $_statusbarStatus.html('Re-importing list of granthas from spreadsheet.....');
        let importUrl = `${$API_URL_PREFIX}import/granthas`;
        $.ajax({
          url:importUrl,
            method: 'POST',
            xhrFields: { withCredentials: true },
          success: () => {
            $_listSearchInput.val("");
            loadGranthas();
          },
          error: () => {
              $_statusbarStatus.text('error in importing grantha list from spreadsheet.');
          }
        });
      
    });

    $_listSearchInput.keyup(function() {
      var start_filter = $_listSearchInput.val().toUpperCase();
      $_shaastraList.children().each(function(i) {
        if ($(this).text().toUpperCase().indexOf(start_filter) == 0) {
          $(this).show();
        }
        else {
          $(this).hide();
        }
      });
    });

    $("#padartha-graph-type").change(function() {
      graph_mode = $("#padartha-graph-type").val();
      let config_section_id = { "default": "#padartha-graph-config", "samanya-vishesha-tree": "#sv-tree-config" }[graph_mode];
      $(".config-section").hide();
      $(config_section_id).show();

      if (graph_mode !== "samanya-vishesha-tree") {
        $("#pg-list-tab0").hide();
        $("#pg-list-tab1").click();
      }
      else {
        $("#pg-list-tab0").show().click();
      }
    })

    $("#pg-list-tab0").click(function() {
      $(".pg-list-tab-title-selected").removeClass("pg-list-tab-title-selected")
      $("#pg-list-tab0").addClass("pg-list-tab-title-selected");
      $("#padartha-graph-nodes-list").hide();
      $("#samanya-vishesha-tree").show();
    });

    $("#pg-list-tab1").click(function () {
      $(".pg-list-tab-title-selected").removeClass("pg-list-tab-title-selected")
      $("#pg-list-tab1").addClass("pg-list-tab-title-selected");
      $("#padartha-graph-nodes-list").show();
      $("#samanya-vishesha-tree").hide();
    });



    $("#si-card-tab-padarthas").click(function() {
      activeTabId = allTabIds[0];
      handleNetworkSwitch();
      resetSelectionSection();
      resetSearchArea();
      if (!padarthasTabShown) {
        let containerElement = $("#si-card-content");
        let plist_wrapper_id = "#padartha-list-title";
        containerElement.animate({
          scrollTop:
            $(plist_wrapper_id).offset().top -
            containerElement.offset().top +
            containerElement.scrollTop()
        });
        padarthaTabCallback(currentShaastraId);
        padarthasTabShown = true;
      }
      else {
        if(!$EVis0) {
          return;
        }
        let selection = $EVis0.network.getSelection();
        if(selection.nodes.length || selection.edges.length) {
          displaySelectionSegment($EVis0, selection);
        }
      }
    });

    $(allTabIds[1]).click(() => {
      activeTabId = allTabIds[1];
      handleNetworkSwitch();
      resetSelectionSection();
      resetSearchArea();
      if (!VakyasTabShown) {
        loadGraph(currentShaastraId);
        VakyasTabShown = true;
      }
      else {
        if (!$EVis1) {
          return;
        }
        let selection = $EVis1.network.getSelection();
        displaySelectionSegment($EVis1, selection);
      }
    });

    $(allTabIds[2]).click(() => {
      activeTabId = allTabIds[2];
      handleNetworkSwitch();
      resetSelectionSection();
      resetSearchArea();
    });

    $("#show-all-padartha-graph").click(function() {
      showAllPadarthaGraph();
      $(".padartha-list-item-selected").removeClass("padartha-list-item-selected");
    });


    $(".showhide").click(function() {
      let listDiv = $(this).siblings("div").eq(0);
      if(listDiv.css("display") == "none") {
        listDiv.show();
      }
      else {
        listDiv.hide();
      }
    });

    $_listStatus.hide();
    $_listErrorReport.hide();
    $_errorReport.hide();
    $_refreshButton.hide();
    $_searchResultsContainer.hide();





    /* Extended Vis Network Global Object Declaration */

    /*fuse object declaration*/
    fuseOptions0 = {
      shouldSort: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.25,
      location: 0,
      distance: 1000,
      maxPatternLength: 128,
      minMatchCharLength: 1,
      keys: [
        "term",
        "definition"
      ]
    };
    fuse0 = null;
    fuseFeed0 = null;

    fuseOptions1 = {
      shouldSort: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.25,
      location: 0,
      distance: 1000,
      maxPatternLength: 128,
      minMatchCharLength: 1,
      keys: [
        "title",
      ]
    };
    fuseFeed1 = null;
    fuse1 = null;

    function normalized_id_part(id) {
        return String(id).replace(/\./g, "_").replace(/:/g, "-")
    }

    /*
    ** Custom implementation of Jquery LoadJson to suit the project's needs
    */
    function loadJSON(url, method, data, beforeSendCallback, successCallback, errorCallback)
    {
      $.ajax({
        method: method,
        data: data,
        dataType: "json",
        url: url,
        xhrFields: { withCredentials: true },
        beforeSend: beforeSendCallback,
        success: successCallback,
        error: errorCallback
      });
    }

    function loadGranthas() {
      var url = apiPrefix + "entities";
      var filter = {
        "jsonClass": "Grantha",
        "Status": "exposed_out"
      };
      var data = $.param({"filter": JSON.stringify(filter)}, true);
      return loadJSON(url, "POST", data, listLoaderCallback, listSuccessCallback, listErrorCallback);
    }

    function loadGraph(bookId) {
      var url = apiPrefix + "graph";
      var base_filter = {
        "jsonClass": "Vakya",
        "Book_id": currentShaastraId
      };
      var traversal_criteria = {
          "jsonClass": "Sambandha"
      }
      var base_criteria = {
          "for": "entities",
          "filter": base_filter
      }
      var data = $.param({
        "base_criteria": JSON.stringify(base_criteria),
        "traversal_criteria": JSON.stringify(traversal_criteria),
        "max_depth": 1
    }, true)
      loadJSON(url, "POST", data, loaderCallback, successCallback, errorCallback)
  }

  function padarthaTabCallback(book_id) {
    if(padarthaResult !== null) {
      renderPadarthaContent();
      return;
    }
    let url = apiPrefix + "graph";
    // let base_filter = { "jsonClass": "Sambandha", "Source_book_id": book_id, "Tantrayukti_tag": { "$in": [" पदार्थः Term definition", "पदार्थः Term definition"]} };
    let base_filter = { "jsonClass": "Sambandha", "Target_book_id": book_id, "$or": [{ "Tantrayukti_tag": { "$regex": "पदार्थः" } }, { "Tantrayukti_tag": { "$regex": "निर्देशः" } }] };
    let base_criteria = {
      "for": "relations",
      "filter": base_filter,
      "options": {
        "include_source_entities": false
      }
    };
    console.log(base_criteria);
    let data = $.param({
      "base_criteria": JSON.stringify(base_criteria),
      "max_depth": 1
    });
    loadJSON(url, 'POST', data,
    function() {
      loaderCallbackTemplate();
    },
    function(result) {
      successCallbackTemplate();
      padarthaResult = {
        entities: Object.assign(new vis.DataSet(result.entities)._data),
        relations: Object.assign(new vis.DataSet(result.relations)._data)
      }
      console.log(padarthaResult);
      padarthaVakyaMap = {};
      Object.values(padarthaResult.relations).forEach((relation, i) => {
        let to =relation['to'];
        if(!(to in padarthaVakyaMap)) {
          padarthaVakyaMap[to] = new Set();
        }
        padarthaVakyaMap[to].add(relation['Source_phrase'].trim());
      });
      console.log(padarthaVakyaMap);
      Object.values(padarthaResult.entities).forEach((entity, i) => {
        if (entity.id in padarthaVakyaMap) {
          entity.explains = [...padarthaVakyaMap[entity.id]];
        }
      });
      let padarthaVakyaEntries = Object.entries(padarthaVakyaMap);
      padarthaVakyaEntries.sort((a, b) => [...a[1]][0].localeCompare([...b[1]][0]) );
      padarthaVakyaMap = new Map(padarthaVakyaEntries);
      renderPadarthaContent(book_id);
    },
    function() {
      errorCallbackTemplate(book_id);
    }
    );
  }

  function renderPadarthaContent(book_id) {
    if(!padarthasRendered) {
      renderPadarthaTerms();
      padarthasRendered = true;;
    }
    if (!padarthaGraphSelectionInfo["padartha_sel_id"]) {
      padarthaGraphSelectionInfo = { padartha_sel_id: `${currentShaastraId}-${"apg"}` };
      showAllPadarthaGraph(book_id, padarthaResult, padarthaGraphSelectionInfo["padartha_sel_id"]);
    }
    
  }

  function padarthaListItemFor(entity) {
      if(!entity) {
          return null;
      }
    return $("<li></li>").attr({ 'id': `pli-${normalized_id_part(entity.id)}`, 'data-node': entity.id, 'data-term': entity.explains.join(';;')}).addClass('padartha-list-item').append($(`<span>${entity.explains.join(', ')}</span>`).addClass('pada-list-text')).append('<br>').append($('<p></p>').addClass('padartha-vakya-item').append($(`<span>${entity.id}: </span>`).addClass('padartha-vakya-id')).append($(`<span>${entity.title}</span>`).addClass('padartha-vakya-title')));
  }

  function renderPadarthaTerms() {
    $('#padartha-list').html('');
    /*Object.values(padarthaResult.entities).forEach((entity, i) => {
      $('#padartha-list').append(padarthaListItemFor(entity));
    });*/
    for(let eid of padarthaVakyaMap.keys()) {
      let entity = padarthaResult.entities[eid];
        let padarthaListItem = padarthaListItemFor(entity);
        if(!padarthaListItem) {
            console.log({eid, entity});
            continue;
        }
      $("#padartha-list").append(padarthaListItemFor(entity));
    }
    fuseFeed0 = Object.values(padarthaResult.entities).map(item => ({
      id: item["id"],
      term: [...item["explains"]].join(", "),
      definition: `${item["id"]} - ${item["title"]}`
    }));
    fuse0 = new Fuse(fuseFeed0, fuseOptions0);

    $(".padartha-list-item").click(function() {
      $("#padartha-graph-nodes-list").html("");
      $("#samanya-vishesha-tree").html("");
      $EVis_svt = null;
      $_networks.eq(0).html("");
      $("#graph-title").html("Term: ");
      let nodeId = $(this).data('node');
      let term = $(this).data('term');
      padarthaGraphSelectionInfo = { "padartha_sel_id": `${currentShaastraId}-padartha-${nodeId}` };
      $(".padartha-list-item-selected").removeClass("padartha-list-item-selected");
      $(this).addClass("padartha-list-item-selected");
      resetSelectionSection();
      showPadarthaSubGraph(nodeId, padarthaGraphSelectionInfo["padartha_sel_id"], term);
    });
  }

  //function onClickForPadarthaNode()

  function padarthasFuseResultsForEach(r) {
    for(let match of r.matches) {
      let highlightedText = `<small>${match['key']}</small>: ${highlightMatch(r.item[match['key']], match.indices)}`;
      $_searchResultsList.append($("<li></li>").addClass("search-result")
        .html(highlightedText).click(function () {
          let pli_id = `#pli-${normalized_id_part(r.item.id)}`;
          let containerElement = $("#padartha-list-div");
          containerElement.animate({
            scrollTop:
              $(pli_id).offset().top -
              containerElement.offset().top +
              containerElement.scrollTop()
          });
          $(pli_id).animate({opacity: "0.1"}, 1);
          $(pli_id).animate({opacity: 1}, 10000);
        }));
    }
  }

  function showPadarthaSubGraph(nodeId, padarthaGraphSelectionId, term) {
    if ($("#padartha-graph-type").val() !== "default") {
      showSamanyaVisheshaGraph(nodeId, padarthaGraphSelectionId, term);
      return;
    }
    let options = deepcopy(__options__);
    let physics_conf = {
      enabled: true
    };
    options['physics'] = physics_conf;
    options.nodes.group = "ndefault";
    let url = apiPrefix + "graph";
    let depth = $("#relation-depth").val() || 0;
    try {
      var custom_traversal_criteria = JSON.parse($("#traversal-criteria").val() || '{}');
    } catch(e) {
      var custom_traversal_criteria = {};
    }
    console.log(custom_traversal_criteria);
    let base_filter = {
      "jsonClass": "Sambandha",
      "$or": [{"from": nodeId}, {"to": nodeId}]
      // "id": nodeId
    };
    let base_criteria = {
      "for": "relations",
      "filter": base_filter
    }
    let traversal_criteria = {
      "jsonClass": "Sambandha"
    };
    for(let key in custom_traversal_criteria) {
      if(custom_traversal_criteria.hasOwnProperty(key)) {
        traversal_criteria[key] = custom_traversal_criteria[key];
      }
    }
    console.log(traversal_criteria);
    let data = $.param({
      "base_criteria": JSON.stringify(base_criteria),
      "traversal_criteria": JSON.stringify(traversal_criteria),
      "max_depth": depth
    });

    loadJSON(url, "POST", data, null, function(result) {
      // console.log(nodeId, padarthaVakyaMap);
      if (padarthaGraphSelectionId !== padarthaGraphSelectionInfo["padartha_sel_id"]) {
        return;
      }
      showPadarthaGraphWithData(result, `Term: ${[...padarthaVakyaMap.get(nodeId)].join(', ')}`, options);
      $EVis0.nodes.update({"id": nodeId, "group": "base"});
      showPadarthaNodesText(result);
    });

  }

  function showAllPadarthaGraph(book_id, prevResult, padarthaGraphSelectionId) {
    let options = deepcopy(__options__);
    let physics_conf = {
      enabled: true,
      maxVelocity: 150,
      minVelocity: 0.5,
      solver: 'barnesHut',
      timestep: 0.35,
      stabilization: { iterations: 100, fit: true }
    };
    options['physics'] = physics_conf;
    options.nodes.group = "disabled";
    if (allPadarthaGraphResult == null) {
      let url = apiPrefix + "graph";
      let base_filter = { "jsonClass": "Sambandha", "Source_book_id": book_id || currentShaastraId, "Tantrayukti_tag": { "$in": [" पदार्थः Term definition", "पदार्थः Term definition"] } };
      let base_criteria = {
        "for": "relations",
        "filter": base_filter,
        "options": {
          "include_base_relations": false
        }
      };
      console.log(base_criteria);
      let data = $.param({
        "base_criteria": JSON.stringify(base_criteria),
        "max_depth": 1
      });
      loadJSON(url, 'POST', data, null, function(result) {
        console.log("apg callback:", padarthaGraphSelectionId, padarthaGraphSelectionInfo);
        allPadarthaGraphResult = {
          entities: result.entities,
          relations: Object.values(prevResult.relations)
        };
        if (padarthaGraphSelectionId !== padarthaGraphSelectionInfo["padartha_sel_id"]) {
          console.log("in apg callback, but not painting");
          return;
        }
        showPadarthaGraphWithData(allPadarthaGraphResult, "All Padartha Usage Graph", options);
        styleAllPadarthaGraph();
        showPadarthaNodesText(allPadarthaGraphResult);
    });
   }
    else {
      showPadarthaGraphWithData(allPadarthaGraphResult, "All Padartha Usage Graph", options);
      styleAllPadarthaGraph();
      showPadarthaNodesText(allPadarthaGraphResult);
    }
  }

  function styleAllPadarthaGraph() {
    let nodeIds = $EVis0.network.body.nodeIndices;
    nodeIds.forEach(nodeId => {
      if(padarthaVakyaMap.has(nodeId)) {
        $EVis0.nodes.update({"id": nodeId, "group": "base"});
      }
      else {
        $EVis0.nodes.update({ "id": nodeId, "group": "disabled" });
      }
    })
  }

  function padarthaGraphCustomAction(params) {
    syncEventWithSVT(params);
    if(params == null) {
      $EVis0.unHighlightNodes();
      resetSelectionSection();
      return;
    }
    if(!params.nodes.length) {
      return;
    }
    let containerElement = $("#padartha-graph-nodes-list-div");
    let itemId = `#padartha-vl-id${normalized_id_part(params.nodes[0])}`;
    containerElement.animate({
      scrollTop:
        $(itemId).offset().top -
        containerElement.offset().top +
        containerElement.scrollTop()
    });
    $(".padartha-vl-item-selected").removeClass("padartha-vl-item-selected");
    $(itemId).addClass("padartha-vl-item-selected");
  }

  function syncEventWithSVT(params) {
    if ($EVis_svt == null || $("#samanya-vishesha-tree").css("display") == "none" || ($("#pg-list-tab0").css("display") == "none")) {
      return;
    }
    if(params == null) {
      $EVis_svt.network.unselectAll();
      return;
    }
    if(params.nodes.length) {
      let nodeId = params.nodes[0];
      let connectedEdges = getEdgesOfNode($EVis0, nodeId, true, false);
      let padas = new Set();
      connectedEdges.forEach(e => {
        let sPada = e["Source_phrase"];
        if (sPada in $EVis_svt.nodes._data) {
          padas.add(sPada);
        }
      });
      console.log(padas);
      $EVis_svt.network.setSelection({
        "nodes": [...padas],
        "edges": []
      });
    }
    else {
      $EVis_svt.network.setSelection({
        "nodes": [],
        "edges": params.edges.filter(e => e in $EVis_svt.edges._data)
      });
    }
  }

  function showPadarthaGraphWithData(data, title, options = __options__) {
    let [graph, network] = showNetwork(data, $_networks[0], options);
    $EVis0 = new EVisNetwork(network, graph, `padarthas-${title}`);
    addEventHandlers($EVis0, padarthaGraphCustomAction);
    graphTitles[0] = title;
    $("#graph-title").text(graphTitles[0]);
  }

  function showPadarthaNodesText(data) {
    let pList = $("#padartha-graph-nodes-list");
    pList.html("");
    data.entities.forEach((entity, i) => {
      pList.append($("<li></li>").addClass('padartha-vl-item').attr({ 'data-node': entity.id, 'id': `padartha-vl-id${normalized_id_part(entity.id)}` }).append($(`<span>${entity.id}: </span>`).addClass('padartha-graph-vakya-id')).append($(`<span>${entity.title}</span>`).addClass('padartha-vakya-title')));
    });
    $(".padartha-vl-item").click(function() {
      let nodeId = $(this).data("node");
      simulateNodeClick($EVis0, nodeId, padarthaGraphCustomAction);
    })
  }


  function showSamanyaVisheshaGraph(nodeId, padarthaGraphSelectionId, term) {

    padarthaGraphSelectionId = padarthaGraphSelectionId+ "_svtree";
    padarthaGraphSelectionInfo["padartha_sel_id"] = padarthaGraphSelectionId;

    let url = apiPrefix + "term_graph";

    let bound_phrase = $("#bound_phrase")[0].checked;
    let same_book = $("#same_book")[0].checked;
    let unbounded_base_entities = $("#unbounded_base_entities")[0].checked;
    let super_cat = $("#super_cat")[0].checked;

    let base_filter = { jsonClass: "Sambandha", Source_phrase: {"$in": term.split(';;')}, $or: [{ Tantrayukti_tag: { $regex: "पदार्थः" } }, { Tantrayukti_tag: { $regex: "निर्देशः" } }] };
    if (same_book || bound_phrase) {
      base_filter["Source_book_id"] = currentShaastraId;
    }
    if(bound_phrase) {
      base_filter[""]
    }
    let base_criteria = {
      "filter": base_filter,
      "extract": 0,
      "include_unbounded_base_entities": unbounded_base_entities? 1: 0
    }

    let traversal_filter = {
      "jsonClass": "Sambandha",
      "Relation_tag": { "$regex": "सामान्य-विशेष-भावः" },
    };
    if(same_book) {
      traversal_filter["Target_book_id"] = currentShaastraId;
    }
    let traversal_criteria = {
      "filter": traversal_filter,
      "direction": 1,
      "bound_phrase": bound_phrase?1: 0
    };

    let data = $.param({
      base_sambandha_criteria: JSON.stringify(base_criteria),
      traversal_criteria: JSON.stringify(traversal_criteria),
      max_depth: 10
    });

    loadJSON(url, "POST", data, null, function(result) {
      if (padarthaGraphSelectionId !== padarthaGraphSelectionInfo["padartha_sel_id"]) {
        return;
      }
      if(super_cat) {
        let super_traversal_criteria = deepcopy(traversal_criteria);
        super_traversal_criteria.direction = 0;
        let super_data = $.param({
          base_sambandha_criteria: JSON.stringify(base_criteria),
          traversal_criteria: JSON.stringify(super_traversal_criteria),
          max_depth: 10
        });
        loadJSON(url, "POST", super_data, null, function(new_result) {
          let combined_result = combineTraversals(result, new_result);
          showSVTreeWithData(term, nodeId, combined_result["result"]);
          combined_result.updated_ids.padas.forEach(p => {
            $EVis_svt.nodes.update({"id": p, "group": "svt-disabled"});
          });
          combined_result.updated_ids.relations.forEach(r => {
            $EVis_svt.edges.update({"id": r, "dashes": true});
          });
        }, function() {
              showSVTreeWithData(term, nodeId, result);
        });
      }
      else {
        showSVTreeWithData(term, nodeId, result);
      }
    });

  }

  function combineTraversals(r1, r2) {
    let r1_entity_ids = r1.entities.map(e => e.id);
    let r1_rel_ids = r1.relations.map(r => r._id);
    let updated_ids = {
      "entities": [],
      "relations": [],
      "padas": []
    }
    console.log(r1_entity_ids, r1_rel_ids);
    let re =deepcopy(r1);
    r2.entities.forEach(e => {
      if(r1_entity_ids.indexOf(e.id) < 0) {
        e.group = "disabled";
        re.entities.push(e);
        updated_ids.entities.push(e.id);
      }
    });
    r2.relations.forEach(r => {
      if (r1_rel_ids.indexOf(r._id) < 0) {
        re.relations.push(r);
        updated_ids.relations.push(r._id);
        if (!(r['Source_phrase'] in re["phrase_relations_map"])) {
          re["phrase_relations_map"][r["Source_phrase"]] = [];
          updated_ids.padas.push(r["Source_phrase"]);
        }
        re["phrase_relations_map"][r["Source_phrase"]].push(r._id);
      }
    });
    return {"result": re, "updated_ids": updated_ids};
  }

  function showSVTreeWithData(term, nodeId, result) {
    let options = deepcopy(__options__);
    let physics_conf = {
      enabled: true
    };
    options['physics'] = physics_conf;
    options.nodes.group = "ndefault";
    /*options.layout["hierarchical"] = {
      //direction: "LR",
      parentCentralization: true,
      //treeSpacing : 700,
      levelSeparation: 200,
      sortMethod: "directed"
    };*/

    showPadarthaGraphWithData(result, `Term: ${[...padarthaVakyaMap.get(nodeId)].join(', ')}`, options);
    let term_relations_map = result["phrase_relations_map"];
    let current_term_relations = term_relations_map[term] || [];
    console.log(term_relations_map, current_term_relations);
    for (let rel of current_term_relations) {
      let relSNodeId = $EVis0.edges.get(rel)['from'];
      $EVis0.nodes.update({ id: relSNodeId, group: "base" });
    }
    // $EVis0.nodes.update({ "id": nodeId, "group": "base" });
    tempObj = result;
    showPadarthaNodesText(result);
    console.log({result, term, nodeId})
    renderSVTree(result);
    $EVis_svt.nodes.update({ "id": term, "group": "svt-base" });
  }


    function renderSVTree(result) {
      RESU = result;
      let nodes = [];
      let edges = [];
      if (!result.relations.length) {
        return;
      }
      for (let pada of Object.keys(result['phrase_relations_map'])) {
        let pada_relations = result["phrase_relations_map"][pada];
        testObj = pada_relations;
        let pada_entity = {
          "id": pada,
          "label": pada,
          "title": pada
        };
        nodes.push(pada_entity);
        for(let rel_id of pada_relations) {
          // console.log(rel);
          let rel = $EVis0.edges.get(rel_id);
          edges.push({
            "_id": rel["_id"],
            "title": rel["title"],
            "label": `${rel['Source_vakya_id']} → ${rel['Target_vakya_id']}`,
            "from": rel["Source_phrase"],
            "to": rel["Target_phrase"]
          });
        }
        
      }
      let computed_result = {
        "entities": nodes,
        "relations": edges
      };

      TestObj = computed_result;

      let options = deepcopy(__options__);
      // options.nodes.group = "ndefault";
      options.layout["hierarchical"] = {
        //direction: "LR",
        parentCentralization: true,
        //treeSpacing : 700,
        levelSeparation: 175,
        edgeMinimization: true,
        sortMethod: "directed"
      };
      options.nodes.shape = "dot",
      options.nodes.size = 10;
      options.nodes.color = {
        background: '#ff0000',
        border: '#3c3c3c',
        highlight: {
          background: '#07f968',
          border: '#3c3c3c'
        }
      };

      options.groups['svt-base'] = {
        color: {
          background: '#f1d98b',
          border: '#3c3c3c',
          highlight: {
            background: '#07f968',
            border: '#3c3c3c'
          }
        }
      };
      options.groups['svt-disabled'] = {
        color: {
          background: '#cccccc',
          border: '#3c3c3c',
          highlight: {
            background: '#07f968',
            border: '#3c3c3c'
          }
        }
      };
      options.nodes.borderWidthSelected = 2;
      delete options.nodes.group;;
      let [graph, network] = showNetwork(computed_result, $("#samanya-vishesha-tree")[0], options);
      $EVis_svt = new EVisNetwork(network, graph, `padarthas-svtree`);
      $EVis_svt.network.on("click", function (selection) {
        $EVis0.network.unselectAll();
        if(!(selection.nodes.length || selection.edges.length)) {
          restoreClickedSelection($EVis0);
          return;
        }
        if(selection.nodes.length) {

          let pada = selection.nodes[0];
          console.log({pada});
          let matchedPadarthaVakyaId = null;
          for(let k of padarthaVakyaMap.keys()) {
            let itsPadas = padarthaVakyaMap.get(k);
            console.log({k, itsPadas, "s": itsPadas.has(pada)});
            if(itsPadas.has(pada)) {
              matchedPadarthaVakyaId = k;
              break;
            }
          }
          console.log({matchedPadarthaVakyaId});
          if(! matchedPadarthaVakyaId) {
            return;
          }
          pliId = `pli-${matchedPadarthaVakyaId.replace(':', '-')}`;
          console.log({pliId});
          $(`#${pliId}`).click();


          /*let pada = selection.nodes[0];
          let related_edge_ids = result["phrase_relations_map"][pada];
          let related_node_ids = related_edge_ids.map(edge_id => $EVis0.edges.get(edge_id)["from"]);
          $EVis0.network.setSelection({
            nodes: related_node_ids,
            edges: []
          });*/
        }
        else {
          let rel_id = selection.edges[0];
          $EVis0.network.setSelection({
            "nodes": [],
            "edges": [rel_id]
          });
        }
      });

    }

    function restoreClickedSelection($EVis) {
      let oldNodes = $EVis.nodes.get({
        filter: function (item) {
          return item.shape === 'box';
        }
      });
      oldNodes.forEach(oldNode => {
        let oldNodeID = oldNode.id;
        let oldEdgesIds = getEdgesOfNode($EVis, oldNodeID);
        $EVis.network.setSelection({
          "nodes": [oldNodeID],
          "edges": oldEdgesIds.map(e => e.id)
        });
      });
    }

    /*
    ** Custom Event Handlers as supported by vis.js and any additional event handlers
    */
    function selectNodeHandler($EVis, params)
    {
      $EVis.highlightNode(params.nodes[0]);
      //$EVis.zoomToNode(params.nodes[0], 1.25);
    }

    function formatJson(json) {
      if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 3);
      }
      /*else {
        json = JSON.stringify(JSON.parse(json), null, 3);
      }*/
      json = json.replace(/^{[^\n]*\n/, "").replace(/}$/, "").replace(/<br>/g, " ;   ");
      json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
      return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
          var cls = 'number';
          if (/^"/.test(match)) {
              if (/:$/.test(match)) {
                match = match.replace(/^"/, "").replace(/":$/, ":").replace(/\n/, " ");
                cls = 'json-format-key';
              } else {
                match = match.replace(/\n/, " ");
                cls = 'json-format-string';
              }
          } else if (/true|false/.test(match)) {
              cls = 'json-format-boolean';
          } else if (/null/.test(match)) {
              cls = 'json-format-null';
          }
          return '<span class="' + cls + '">' + match + '</span>';
      });
    }

    function cardForVakyaNode($EVis, nodeId) {
      var data = $EVis.nodes.get(nodeId);
      var card = $("<div></div>").addClass("card node-vakya-card col s12").attr({ "id": "sel-v" + normalized_id_part(nodeId)})
                              .append($("<span></span>").addClass("card-title node-vakya-card-title").html("Vakya:&nbsp;" + nodeId))
                              .append($("<div></div>").addClass("card-content node-vakya-card-content").append($("<p></p>").addClass("json-pre").html(formatJson(JSON.stringify(data, Helper.excludeFields, 3)))));
      return card;
    }

    function cardForSambandhaEdge($EVis, edgeId) {
      var data = $EVis.edges.get(edgeId);
      if(!data) {
        return null;
      }
      var srcvakya = $EVis.nodes.get(data['from']);
      var destvakya = $EVis.nodes.get(data['to']);
      if(!srcvakya || !destvakya) {
        return null;
      }

      var card = $("<div></div>").addClass("card edge-vakya-card col s12").attr({"id" : "sel-l"+edgeId})
                              .append($("<span></span>").addClass("card-title edge-vakya-card-title").html("Sambandha:&nbsp;" + data["from"] + " → " + data["to"]))
                              .append($("<div></div>").addClass("card-content edge-vakya-card-content")
                                  .append($("<p></p>").addClass("edge-vakya-content-card-vakyas").html("<span class=\"vakya-id\">"+ data['from'] + "</span>" + ": " + JSON.stringify(srcvakya['title'])))
                                  .append($("<p></p>").addClass("edge-vakya-content-card-vakyas").html("<span class=\"vakya-id\">"+ data['to'] + "</span>" + ": " + JSON.stringify(destvakya['title'])))
                                  .append($("<p></p>").addClass("json-pre").html(formatJson(JSON.stringify(data, Helper.excludeFields, 3)))));
      return card;
    }


    function handleClusterClick($EVis, params) {
        nodesList = params.nodes || [];
        let edgesList = params.edges || [];
        if (nodesList) {
            if ($EVis.network.isCluster(nodesList[0])) {
                try {
                    $EVis.network.openCluster(nodesList[0]);
                }
                catch {}
                return true;
          }
        }
        if (edgesList && edgesList[0]) {
          if (edgesList[0].startsWith("clusterEdge:")) {
            return true;
          }
        }
    }

    function networkClickHandler($EVis, params, customAction)
    {
        networkSelectionItems = params;

      if(!(params.nodes.length || params.edges.length)) {
        if(customAction) {
          customAction(null);
        }
        return;
      }
      if(!(params.nodes.length)) {
        $EVis.unHighlightNodes();
      }

      displaySelectionSegment($EVis, params);
      if(customAction) {
        customAction(params);
      }
    }

    function displaySelectionSegment($EVis, params) {
        
      $("#content").show();
      networkSelectionInfo = {"selection_id": `${$EVis.networkId}:n${params.nodes.join(',')}:e${params.edges.join(',')}`};
      displaySelectionNetwork($EVis, params);
      handleNodesClick($EVis, params);
      handleEdgesClick($EVis, params);
    }

    function displaySelectionNetwork($EVis, params) {
      $("#ci-network").html("");
      if(params.nodes.length) {
        let networkSelectionId = networkSelectionInfo["selection_id"];
        let url = apiPrefix + "graph";
        base_filter = {
          "jsonClass": "Sambandha",
          "$or": [{ "from": { "$in": params.nodes } }, { "to": { "$in": params.nodes }}]
        };
        base_criteria = {"for": "relations", "filter": base_filter}
        depth = 0;
        traversal_criteria = {"jsonClass": "Sambandha"};
        let data = $.param({"base_criteria": JSON.stringify(base_criteria), "max-depth": depth});
        loadJSON(url, "POST", data, null, function(result) {
          renderSelectionNetworkWithData($EVis, params, result, 'Selected entity\'s friend circle', networkSelectionId);
        })
      }
      else if(params.edges.length){
        let computedNodes = [];
        params.edges.forEach(edgeId => {
          // computedNodes.push(...getNodesOfEdge($EVis, edgeId));
          computedNodes.push(...([...new Set($EVis.network.getConnectedNodes(edgeId))].map(nodeId => $EVis.nodes.get(nodeId))));
        });
        let computedResult = {
          "entities": computedNodes,
          "relations": params.edges.map(edgeId => $EVis.edges.get(edgeId))
        };
        console.log(computedResult);
        renderSelectionNetworkWithData($EVis, params, computedResult, 'Selected relation', networkSelectionInfo["selection_id"]);
      }
    }

    function renderSelectionNetworkWithData($EVis, params, result, title, oldNetworkSelectionId) {
      if(oldNetworkSelectionId !== networkSelectionInfo["selection_id"]) {
        return;
      }
      let [graph, network] = showNetwork(result, $("#ci-network")[0], deepcopy(__options__));
      $EVis_sel = new EVisNetwork(network, graph, 'selection-network');
      $("#ci-network").append($('<span></span>').addClass("custom-title").text(title).css({"position": "absolute", "top": "0.2em", "left": "0.2em", "font-size": "x-small"}));
      let allNodes = $EVis_sel.nodes.get();
      allNodes.forEach(node => {
        if (node.id in $EVis.nodes._data) {
          $EVis_sel.nodes.update({"id": node.id, "group": "ndefault"});
        }
        else {
          $EVis_sel.nodes.update({ "id": node.id, "group": "disabled" });
        }
      })
      params.nodes.forEach(nodeId => {
        $EVis_sel.nodes.update({"id": nodeId, "group": "base"});
      });
      $EVis_sel.network.on("click", getSelectionNetworkClickHandler(params));
    }

    function scrollToElement(containerElement, specificElement) {

    }

    function getSelectionNetworkClickHandler(params) {
      return function selectionNetworkClickHandler(selection) {
        $EVis_sel.unHighlightNodes();
        if (!(selection.nodes.length || selection.edges.length)) {
          $EVis_sel.unHighlightNodes();
          return;
        }
        if(!(selection.nodes.length)) {
          let edgeId = selection.edges[0];
          if(params.edges.indexOf(edgeId) < 0) {
            $EVis_sel.network.unselectAll();
          }
          else {
            indexItemClickHandler(edgeId, "edge");
          }
        }
        else {
          let nodeId = selection.nodes[0];
          if(params.nodes.indexOf(nodeId) < 0) {
            $EVis_sel.network.unselectAll();
            $EVis_sel.unHighlightNodes();
          }
          else {
            $EVis_sel.highlightNode(nodeId);
            indexItemClickHandler(nodeId, "node");;
          }
        }
      }
  }

    function handleNodesClick($EVis, params) {
      let nodeIds = params.nodes;
      $_vakyaContentNodes.html("");
      let nodesIndexList = $("#ci-nodes-list");
      $("#sel-count-nodes").html(nodeIds.length);
      nodesIndexList.html("");

      nodeIds.forEach(function(nodeId) {
        let data = $EVis.nodes.get(nodeId);
        $_vakyaContentNodes.append(cardForVakyaNode($EVis, nodeId));
        nodesIndexList.append($("<li></li>")
            .attr({ "data-node": nodeId, "title": data.title.replace(/<br>/g, '\n'), "id": `cii-n${normalized_id_part(nodeId)}` })
            .addClass("content-index-item node-index-item")
            .append($("<span></span>")
              .addClass("content-index-item-id node-index-item-id")
                .text(nodeId))
            .append($("<br>"))
          .append($("<span></span>").addClass("content-index-item-label node-index-item-label").text(`${data.title}`))
            );
      });

      $(".node-index-item").click(function() {
        let nodeId = $(this).data("node");
        indexItemClickHandler(nodeId, "node");
        $EVis_sel.highlightNode(nodeId);
        $EVis_sel.zoomToNode(nodeId, 0.7);
        var edgesOfNode = getEdgesOfNode($EVis_sel, nodeId);
        var edgeIds = edgesOfNode.map((edge) => edge["id"]);
        let selection = { nodes: [nodeId], edges: edgeIds };
        $EVis_sel.network.setSelection(selection);

      });

    }

    function indexItemClickHandler(itemId, itemType) {
      let containerElement = $("#content-ne");
      let itemCardId = { "node": `#sel-v${normalized_id_part(itemId)}`, "edge": `#sel-l${itemId}`}[itemType];
      $(".content-index-item-selected").removeClass("content-index-item-selected");
      let indexItemId = { node: `#cii-n${normalized_id_part(itemId)}`, edge: `#cii-e${itemId}` }[itemType];
      $(indexItemId).addClass("content-index-item-selected");
      containerElement.animate({
        scrollTop: $(itemCardId).offset().top - containerElement.offset().top + containerElement.scrollTop()
      });
    }

    function handleEdgesClick($EVis, params) {
      let edgeIds = params.edges;
      $_vakyaContentEdges.html("");
      let edgeIndexList = $("#ci-edges-list");
      $("#sel-count-edges").html(edgeIds.length);
      edgeIndexList.html("");

      edgeIds.forEach(function (edgeId) {
        var data = $EVis.edges.get(edgeId); // get data from selected edge
        let seCard = cardForSambandhaEdge($EVis, edgeId);
        if(!seCard) {
          return;
        }
        $_vakyaContentEdges.append(cardForSambandhaEdge($EVis, edgeId));
        edgeIndexList.append($("<li></li>")
            .attr({
              "data-edge": edgeId,
              title: data["title"].replace(/<br>/g, "\n"),
              id: `cii-e${edgeId}`
            })
            .addClass("content-index-item edge-index-item")
            .append($("<span></span>")
                .addClass("content-index-item-id edge-index-item-id")
                .text(`${data["from"]} → ${data["to"]}`))
            .append($("<br>"))
            .append($("<span></span>")
                .addClass("content-index-item-label edge-index-item-label")
                .text(`${data["label"]}`)));
      });

      $(".edge-index-item").click(function() {
        let edgeId = $(this).data("edge");
        $EVis_sel.unHighlightNodes();
        let selection = { nodes: [], edges: [edgeId] };
        $EVis_sel.network.setSelection(selection);
        indexItemClickHandler(edgeId, "edge");
      });
    }

    function simulateNodeClick($EVis, nodeId, customAction) {
      let foundNodes = $EVis.network.findNode(nodeId);
      if(!foundNodes.length) {
        return;
      }
      if(foundNodes.length == 1) {
        $EVis.highlightNode(nodeId);
        var preScale = $EVis.network.getScale();
        $EVis.zoomToNode(nodeId, 0.7);
        
        $EVis.network.setSelection({
          'nodes': [nodeId,],
          'edges': []
        });
      }
      else {
        let baseClusterId = foundNodes[0];
        // $EVis.highlightNode(baseClusterId);
        $EVis.zoomToNode(baseClusterId, 0.7);
        $EVis.network.setSelection({
          'nodes': [baseClusterId]
        });
      }
      var edgesOfNode = getEdgesOfNode($EVis, nodeId);
      var edgeIds = [];
      edgesOfNode.forEach(function (edge) {
        edgeIds.push(edge["id"]);
      });
      networkClickHandler($EVis, {
        "nodes" : [nodeId,],
        "edges" : edgeIds
      }, customAction); //now we are not using any meta data from actual click event in networkClickHandler. so we can pass our object with only avialble params.
    }

    function highlightMatch(text, matches) {
      var result = [];
      var devaAch = "ािीुूृॄॢॣेैोौ्ंःँ";
      var pair = matches.shift();
      // Build the formatted string
      for (var i = 0; i < text.length; i++) {
        var char = text.charAt(i);
        if (pair && i == pair[0]) {
          if(devaAch.indexOf(char) > -1) {
            pair = matches.shift();
          }
          else {
            result.push('<span class="search-match">');
          }
        }
        result.push(char)
        if (pair && i == pair[1]) {
          result.push('</span>');
          pair = matches.shift();
        }
      }
      return result.join('');
    }


    $_searchInput.keyup(function() {
      /*if(activeTabId !== allTabIds[1]) {
        return;
      }*/
      $_searchResultsList.html("");
      var sword = $_searchInput.val().trim();
      if(!sword) {
        $_searchResultsContainer.hide();
        return;
      }
      else {
        $_searchResultsContainer.show();
      }
      let fuse = [fuse0, fuse1, null][allTabIds.indexOf(activeTabId)];
      if(fuse == null) {
        return;
      }
      sresults = fuse.search(sword)
      if (!sresults.length) {
        $_searchResultsStatus.html("").append($("<span></span>").addClass('no-search-results').text('no results found for ' + sword));
        return;
      }
      else {
        $_searchResultsStatus.html("").append($("<span></span>").addClass('yes-search-results').text(sresults.length + ' close results found for ' + sword));
      }
      let resForEach = [padarthasFuseResultsForEach, vakyasFuseResultsForEach, function(){}][allTabIds.indexOf(activeTabId)];
      sresults.forEach(resForEach);
    });

    /*
    ** Tasks to be performed if JSON is successfully loaded.
    ** These tasks must be performed in the successCallback in the order in which function definitions are written.
    */
    function getEdgesOfNode($EVis, nodeId, source=true, target=true) {
      var edgesOfNode = $EVis.edges.get().filter(function(edge) {
          return ((edge.from == nodeId) && source) || ((edge.to == nodeId) && target);
        });
      return edgesOfNode;
    }

    function showShaastraText(data, shaastraTextJQObject)
    {
      var JQObj = shaastraTextJQObject;
      JQObj.html("");
      fuseFeed1 = []; //for search
      data.entities.forEach(function(item){
        fuseFeed1.push({id: item["id"], title: item["id"] + ": " + item["title"]});
        JQObj.append($("<p></p>").addClass("vakya-link-pg").attr({"data-node" : item["id"], "id" : "vakya-link-pg-id" + normalized_id_part(item["id"])})
          .append($("<span></span>").addClass("vakya-id").html(item["id"] + ":&nbsp;&nbsp;&nbsp;"))
          .append($("<span></span>").addClass("vakya-link").attr({"data-node" : item["id"]}).html(item["title"]))
          );
      });
      fuse1 = new Fuse(fuseFeed1, fuseOptions1); //for search

      $('.vakya-link-pg').click(function(){
        var nodeId = $(this).data('node');
        simulateNodeClick($EVis1, nodeId, vakyaGraphCustomAction);
      });
    }

    function vakyasFuseResultsForEach(r) {
      let highlightedTitle = highlightMatch(r.item["title"], r.matches[0].indices);
      $_searchResultsList.append($("<li></li>").addClass("search-result")
        .html(highlightedTitle).click(function () {
          simulateNodeClick($EVis1, r.item["id"], vakyaGraphCustomAction);
        }));
    }

    function showNetwork(data, networkContainer, options)
    {
      networkContainer.innerHTML = "";
      temporaryIdFix(data.relations);
      var graph = {
        nodes: new vis.DataSet(data.entities),
        edges: new vis.DataSet(data.relations)
      };

      var network = new vis.Network(networkContainer, graph, Object.create(options));
      return [graph, network];
    }

    function temporaryIdFix(edges) {
      edges.forEach(edge => {
          if(edge && '_id' in edge) {
              edge['id'] = edge['_id'];
          }
        
      });
    }

    

    function addEventHandlers($EVis, customAction)
    {
      $EVis.network.on("selectNode", function(params) {
          let is_cluster = handleClusterClick($EVis, params);
          if(is_cluster) {
              return;
          }
        selectNodeHandler($EVis, params);
      });
      $EVis.network.on('click', function(params) {
          let is_cluster = handleClusterClick($EVis, params);
          if(is_cluster) {
              return;
          }
        networkClickHandler($EVis, params, customAction);
      });
      //$_searchInput.on('keydown', searchInputKeydownHandler);
    }
    /*
    ** Function callbacks for asynchronous request of JSON
    */

    function loaderCallbackTemplate() {
      $_statusbarStatus.text("loading " + id2ShaastraMap[currentShaastraId]["Book_title"] + " smap");
      $_contentArea.hide();
      $_errorReport.html("").hide();
      $_preloader.show();
      $_refreshButton.show();
      //reset vakya content.
      $_vakyaContentNodes.html('');
      $_vakyaContentEdges.html('');
      resetSearchArea();
    }

    function loaderCallback()
    {
      loaderCallbackTemplate();
    }


    function successCallbackTemplate(result) {
      $_preloader.hide();
      $_errorReport.html("").hide();
      $_contentArea.show();
      $_refreshButton.show();
      $_statusbarStatus.html("").append($("<span></span>").text(id2ShaastraMap[currentShaastraId]["Book_title"]).addClass("shaastra-name"))
        .append($("<a></a>").attr("href", docsUrlForSpreadsheet(id2ShaastraMap[currentShaastraId]["Spreadsheet_id"])).attr("target", "_blank").text("  🔗 ").addClass("docs-external-link"));
    }

    function vakyaGraphCustomAction(params) {
      if(params == null) {
        $EVis1.unHighlightNodes();
        resetSelectionSection();
        return;
      }
      if (!params.nodes.length) {
        return;
      }
      let containerElement = $("#si-card-content");
      let itemId = `#vakya-link-pg-id${normalized_id_part(params.nodes[0])}`;
      containerElement.animate({
        scrollTop:
          $(itemId).offset().top -
          containerElement.offset().top +
          containerElement.scrollTop()
      });
      $(".vakya-link-pg-selected").removeClass("vakya-link-pg-selected");
      $(itemId).addClass("vakya-link-pg-selected");
    }

    function successCallback(result)
    {
      let options = deepcopy(__options__);
      let physics_conf = {
        enabled: true,
        maxVelocity: 50,
        minVelocity: 2,
        solver: 'barnesHut',
        timestep: 0.35,
        stabilization: { iterations: 50, fit: true }
      };
      options['physics'] = physics_conf;
      options.nodes.group = "ndefault";
      successCallbackTemplate(result);
      showShaastraText(result, $_shaastraText);
      try {
          let [graph, network] = showNetwork(result, $_networks[1], options);
          $EVis1 = new EVisNetwork(network, graph, "vakyas-all")
          $EVis1.clusterBySequences();
          addEventHandlers($EVis1, vakyaGraphCustomAction);
      } catch (e) {
          $_networks.html('<small>error in creating network. may be there are duplicate ids: <br>'+ e+ "</small>")
      }
    }

    function errorCallbackTemplate(xhr, status, error) {
      $_statusbarStatus.html("").append("error in loading ")
        .append($("<span></span>").text(id2ShaastraMap[currentShaastraId]["Book_title"]).addClass("shaastra-name"))
        .append($("<a></a>").attr("href", docsUrlForSpreadsheet(id2ShaastraMap[currentShaastraId]["Spreadsheet_id"])).attr("target", "_blank").append($("<sup></sup>").text("  🔗 ")).addClass("docs-external-link"))
        .append(' smap');
      $_preloader.hide();
      $_contentArea.hide();
      $_errorReport.html('').append($("<pre></pre>").text(xhr.responseText)).show();
    }

    function errorCallback(xhr, status, error) {
      errorCallbackTemplate();
    }

    /*
    function callbacks for asynchronous request of shaastra list json
    */

    function listLoaderCallback()
    {
      $_shaastraList.html("");
      id2ShaastraMap = {};

      if(!currentShaastraId) {
        $_contentArea.hide();
        $_preloader.show();
        $_statusbarStatus.text("loading list of granthas.......");
      }
      $_listErrorReport.hide();
      $_shaastraList.hide();
      $_listStatus.text('loading granthas list........').show();
    }

    function listSuccessCallback(result) {
      if(!currentShaastraId) {
        $_preloader.hide();
        $_errorReport.html("").hide();
        //$_contentArea.show()
        $_statusbarStatus.text("loading list of granthas... done.");
      }

      granthas = result.sort((g1,g2) => (g1.Book_title > g2.Book_title) ? 1 : -1)
      for (g of granthas) {
        id2ShaastraMap[g.Book_id] = g;
        list_item_link = $("<a></a>")
                                        .attr("href", "javascript:void(0)")
                                        .attr({"title": g.Book_title, "id": `grantha-${g.Book_id}`})
                                        .click(onclickForListItem(g))
                                        .text(g.Book_title)
                                        .addClass("truncate");
        shaastra_list_item = $("<li></li>")
                                              .append(list_item_link);
        $_shaastraList.append(shaastra_list_item);
      }

      if(!currentShaastraId) {
        $_statusbarStatus.text("shaastras list successfully loaded to side-navbar.");
        $('.button-collapse').sideNav('show');
      }
      $_listStatus.hide();
      $_listErrorReport.hide();
      $_shaastraList.show();
    }

    function listErrorCallback(xhr, status, error) {
      if(!currentShaastraId) {
        $_statusbarStatus.text("error in loading shaastras list");
        $_preloader.hide();
        $_contentArea.hide();
        $_errorReport.html('').append($("<pre></pre>").text(xhr.responseText)).show();
      }
      $_listStatus.text('error in loading shaastras list. fallowing are error details...').show();
      $_shaastraList.hide();
      $_listErrorReport.html($("<pre></pre>").text(xhr.responseText)).show();
    }

    /*
    ** Initial Function Call
    */
    //loadJSON("assets/feed/json/arthashaastra-graph.json", loaderCallback, successCallback);
    loadJSON(REPO_URL+'/list', "GET", null, function () {
        $_repoSetter.html("");
        $_repoSetter.show();
        if(!currentShaastraId) {
          $_contentArea.hide();
          $_preloader.show();
          $_statusbarStatus.text("loading list of repos.......");
        }

    }, function(result){
        if(!currentShaastraId) {
          $_preloader.hide();
          $_errorReport.html("").hide();
          //$_contentArea.show()
        }
        $_repoSetter.append($("<option></option>").attr({'value': "", "disabled":"true", "selected": "true"}).text("select repo").hide())
        result['repos'].forEach(repo_name => {
            $_repoSetter.append(new Option(repo_name, repo_name));
            //$_repoSetter.append($("<option></option>").attr('value', repo_name).text(repo_name));
        })
        if(!currentShaastraId) {
          $_statusbarStatus.text("repos list succefully loaded to side-navbar. select one.");
          $('.button-collapse').sideNav('show');
        }

    }, function(xhr, status, error) {
        $_repoSetter.html("");
        $_statusbarStatus.text("error in getting repos list");
        $_preloader.hide();
        $_contentArea.hide();
        $_errorReport.html('').append($("<pre></pre>").text(xhr.responseText)).show();
    });
    //loadGranthas();
  });
})(jQuery);
